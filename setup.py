from setuptools import find_packages, setup
from pathlib import Path

with open(Path("src/README.md")) as f:
    long_description = f.read()

setup(
    name="libthora",
    version="0.0.1",
    description="A library that contains a simple implementation of the Tenbrink algorithm",
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/prvInSpace/libthora",
    author="Preben Vangberg",
    author_email="prebenvangberg@gmail.com",
    license="MIT",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.11",
        "Operating System :: OS Independent"
    ],
    install_requires=[],
    extras_requires=[],
    python_requires=">=3.11"
)
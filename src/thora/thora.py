
from dataclasses import dataclass
import re

@dataclass
class Indicator:
    """Represents an `indicator` which is defined as a set
    of words and a weight. The weight can either be an int or a float"""
    weight: int | float
    word_list: list


def count_word_occurances(word_list: list[Indicator], text: str, case_insensitive: bool = True) -> int:
    """Counts the amount of times the words in the word list are found in
    the given text"""
    pattern = re.compile(
        rf"\b({'|'.join(map(re.escape, word_list))})\b",
        flags=re.IGNORECASE if case_insensitive else re.NOFLAG
    )
    return len(pattern.findall(text))


def apply_thora(indicators: list[Indicator], text: str, case_insensitive: bool = True) -> int | float:
    """Applies the Tenbrink codescheme analysis algorithm to the text"""
    return sum([
        count_word_occurances(ind.word_list, text) * ind.weight
        for ind in indicators
    ])

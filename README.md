# LibThora

A packaged used to make it easier to apply the Tenbrink codescheme analysis algorithm to your text.

## How to install

Currently the library is only hosted on the testing repo for PyPi, and as such it can be installed by running the following command:

`pip install -i https://test.pypi.org/simple/ libthora`

## How to use

After installing the package simply import the `Indicator` class and the `apply_thora` method.
The `Indicator` class represents your word lists and their weights and can be constructed like in the following example.

```python3
my_indicator = Indicator(
    2, # This is your weigth
    ["word1", "word2", "word3"] 
)
```

Then you can simply run the algorthim like so

```python3
indicators = [my_indicator]
text = "Some test text which happens to contain word1"
apply_thora(indicators, text)
```

In which case the algorithm should return the number 2.

## Maintainers

* Preben Vangberg &lt;prv21fgt@bangor.ac.uk&gt;

FROM python:3.11-slim

# Install requirements
RUN python3 -m pip install --upgrade pip 
ADD requirements.txt /requirements.txt
RUN pip install -r requirements.txt

# Setup the user profile
ARG USERNAME=prv21fgt
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    # Add sudo support.
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

# Install fish and neovim
RUN apt install -y fish git neovim

# Change the default shell
RUN chsh -s /usr/bin/fish $USERNAME

# Set the default user. Omit if you want to keep the default as root.
USER $USERNAME

# Install autopep8 as the user
RUN python3 -m pip install autopep8

# Download the config files 
RUN git clone https://gitlab.com/prvInSpace/uniplexed-commafiles ~/.config